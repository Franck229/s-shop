import React, { useEffect, useState } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Col, Row } from 'reactstrap';

//import Products from "./pages/Products";
import Home from "./components/Home";
import Cart from "./components/Cart";


const PAGE_HOME = "home";
const PAGE_CART = "cart";

const cartFromLocalStorage = JSON.parse(localStorage.getItem("cart") || "[]");

function App() {
  const [cart, setCart] = useState(cartFromLocalStorage);
  const [page, setPage] = useState(PAGE_HOME);

  useEffect(() => {
    // localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);
  const navigateTo = (nextPage) => {
    setPage(nextPage);
  };

  const getCartTotal = () => {
    return cart.reduce((sum, { quantity }) => sum + quantity, 0);
  };


  return (
    <div className="App" >
      <header >
        <button onClick={
          () => navigateTo(PAGE_CART)
        } >
          Cesta ({getCartTotal()}) {" "} </button>
        {" "} <button onClick={
          () => navigateTo(PAGE_HOME)
        } >
          Home{" "}
        </button>{" "}
      </header > {" "} {
        page === PAGE_HOME && <Home cart={cart}
          setCart={setCart}
        />}{" "} {
        page === PAGE_CART && < Cart cart={cart}
          setCart={setCart}
        />}{" "}
    </div>
  );

}
export default App;
