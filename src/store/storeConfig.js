import { createStore, combineReducers } from "redux";

const reducers = combineReducers({


    purchases: function(state, action) {

        switch (action.type) {
            case 'ALTER_ClIENT_NAME':
                return {
                    ...state,
                    clientName: action.payload
                }
            case 'ALTER_PURCHASES':
                return {
                    ...state,
                    compras: action.payload
                }

            default:
                return {
                    clientName: '',
                    compras: []
                }
        }


    },


})

function storeConfig() {
    return createStore(reducers)
}
export default storeConfig