//Action Creator
export function alterPurchasesClientData(newPurchasesClient) {
    return {
        type: 'ALTER_ClIENT_NAME',
        payload: newPurchasesClient
    }
}

export function alterPurchasesData(newPurchases) {
    return {
        type: 'ALTER_PURCHASES',
        payload: newPurchases
    }
}