import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import Admin from './components/Admin';
import { Switch, Route, HashRouter } from "react-router-dom";
import { Provider } from 'react-redux'
import storeConfig from './store/storeConfig'
const store = storeConfig()
ReactDOM.render( <
    Provider store = { store } >
    <
    HashRouter >
    <
    Switch >
    <
    Route path = "/"
    exact = { true }
    component = { App }
    /> 

    <
    Route path = "/Admin"
    component = { Admin }
    /> </Switch >
    <
    /HashRouter > 

    <
    /Provider>,
    document.getElementById('root')
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();