import React, { useState } from 'react';
import * as Constants from '../constants';



export default function Home({ setCart, cart }) {

	const [products] = useState(Constants.data);

	const addToCart = (product) => {
		let newCart = [...cart];
		let itemInCart = newCart.find(
			(item) => product.name === item.name
		);
		if (itemInCart) {
			itemInCart.quantity++;
		} else {
			itemInCart = {
				...product,
				quantity: 1,
			};
			newCart.push(itemInCart);
		}
		setCart(newCart);
	};

	const [category, setCategory] = useState(Constants.HOME_GARDEN);

	const getProductsInCategory = () => {
		return products.filter(
			(product) => product.category === category
		);
	}
	return (
		<div className="">
			<h1>Home</h1>
			Select a category
			<select onChange={(e) => setCategory(e.target.value)}>
				<option value={Constants.HOME_GARDEN}>{Constants.HOME_GARDEN}</option>
				<option value={Constants.UTILITY}>{Constants.UTILITY}</option>

			</select>
			<table class="table caption-top">
				<caption>Produtos</caption>

				<thead>
					<tr>
						<th scope="col">Produto</th>
						<th scope="col">Imagem</th>
						<th scope="col">Descrição</th>
						<th scope="col">Quantidade</th>




						<th scope="col"> </th>
					</tr>
				</thead>
				<tbody>
					{getProductsInCategory().map((product, idx) => (


						<tr>
							<th scope="row" key={idx} >{product.name}</th>
							<td>  <img src={product.image} alt={product.name} width="50" /></td>


							<td>{product.description}</td>
							<td >{product.quantity}</td>

							<td>  <button onClick={() => addToCart(product)}>
								Adicionar
							</button></td>
						</tr>



					))}
				</tbody>



			</table>

		</div>
	);;
}
