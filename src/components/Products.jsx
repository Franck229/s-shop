import React, { useState } from 'react';
import * as Constants from '../constants';
import "./Products.css";


export default function Products({ setCart, cart }) {

	const [products, setProducts] = useState(Constants.data);
	console.log(" state products", products);
	const addToCart = (product) => {
		console.log("newCart product", product);
		let newCart = [...cart];
		console.log("newCart", newCart);
		let itemInCart = newCart.find(
			(item) => product.name === item.name
		);
		if (itemInCart) {
			itemInCart.quantity++;
		} else {
			itemInCart = {
				...product,
				quantity: 1,
			};
			newCart.push(itemInCart);

		}
		setCart(newCart);
	};

	const addToProducts = (product) => {
		console.log("newProducts product", product);
		let newProducts = [...products];

		console.log("newProducts", newProducts);
		let itemInProducts = newProducts.find(
			(item) => product.name === item.name
		);
		if (itemInProducts) {
			itemInProducts.quantity++;
		} else {
			itemInProducts = {
				...product,
				quantity: 1,
			};
			var removed = newProducts.splice(19, 0, itemInProducts);

		}

		setProducts(newProducts);
	};
	const removeFromCart = (productToRemove) => {
		setProducts(
			products.filter((product) => product !== productToRemove)
		);
	};

	const [category, setCategory] = useState(Constants.HOME_GARDEN);

	const getProductsInCategory = () => {
		return products.filter(
			(product) => product.category === category
		);
	}

	const [categoryToAdd, setCategoryToAdd] = useState(Constants.HOME_GARDEN);

	var NameTemp, DescricaoTemp, PrecoTemp = "";
	const HandleChangeName = (newNome) => {

		NameTemp = newNome;
	};
	const HandleChangeDescricao = (newDescricao) => {

		DescricaoTemp = newDescricao;
	};
	const HandleChangePreco = (newPreco) => {

		PrecoTemp = newPreco;
	};
	const addingProducto = () => {
		if (NameTemp === "" || DescricaoTemp === "" || PrecoTemp === "") {
			alert("Nenhum campo pode ficar em branco");
		} else {

			const dataTemp = {
				category: categoryToAdd,
				name: NameTemp,
				quantity: 1,
				description: DescricaoTemp,
				cost: parseFloat(PrecoTemp),
				image: categoryToAdd === Constants.UTILITY ? 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ5-QAul_NfAs-s0XW9M087xWyPOGWvbfYjmqSl0QXabZRSYoid47i7kISiAteyIh0YOci5mtQ&usqp=CAc' : 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpwdYDmUL_ZEqhLV7ZWRdQAU7DGcGaxtCt7SrTlL9umrQs2Un7rj9Nbb9Vq01RtEfA0eAVmdt-&usqp=CAc',
			};


			addToProducts(dataTemp);

			alert("Produto adicionado com sucesso realzada com sucesso")

		}
		return;
	};

	return (
		<div className="">


			<div className="imputGrup"> <h2>Catehoria do Produtos</h2>
				<select onChange={(e) => setCategory(e.target.value)}>
					<option value={Constants.HOME_GARDEN}>{Constants.HOME_GARDEN}</option>
					<option value={Constants.UTILITY}>{Constants.UTILITY}</option>

				</select>
			</div>



			<div className="imputGrup">
				<select onChange={(e) => setCategoryToAdd(e.target.value)}>
					<option value={Constants.HOME_GARDEN}>{Constants.HOME_GARDEN}</option>
					<option value={Constants.UTILITY}>{Constants.UTILITY}</option>

				</select>
				<input

					name="Name"
					type={Text}
					placeholder="Nome"
					onChange={(event) => HandleChangeName(event.target.value)}
				/>
				<input

					name="Descricao"
					type={Text}
					placeholder="Descricao"
					onChange={(event) => HandleChangeDescricao(event.target.value)}
				/>
				<input

					name="Preco"
					type={Text}
					placeholder="Preco"
					onChange={(event) => HandleChangePreco(event.target.value)}
				/>
				<button onClick={() => addingProducto()}>
					Add Product
				</button>
			</div>
			<table class="table caption-top">
				<caption>		<h3>Products</h3></caption>

				<thead>
					<tr>
						<th scope="col">Produto</th>
						<th scope="col">Imagem</th>
						<th scope="col">Descrição</th>
						<th scope="col">Preço</th>


						<th scope="col">Adicionar</th>
					</tr>
				</thead>
				<tbody>
					{getProductsInCategory().map((product, idx) => (




						<tr>
							<th scope="row" key={idx} >{product.name}</th>
							<td>  <img src={product.image} alt={product.name} width="50" /></td>


							<td>{product.description}</td>
							<td>R${product.cost}</td>

							<td>  <button onClick={() => removeFromCart(product)}>
								Remove
							</button></td>
						</tr>



					))}
				</tbody>



			</table>

		</div>
	);;
}
