import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import PurchasesTable from './PurchasesTable';
import { Container, Col, Row } from 'reactstrap';
//import Products from "./pages/Products";
import Cart from "./Cart";
import Produtos from "./Products";
import Compras from "./Compras";

const PAGE_PRODUCTS = "produtos";
const PAGE_PURCHASES = "compras";

const cartFromLocalStorage = JSON.parse(localStorage.getItem("cart") || "[]");
const comprasFromLocalStorage = JSON.parse(localStorage.getItem("compras") || "[]");
const clientNameFromLocalStorage = JSON.parse(localStorage.getItem("clientName") || "[]");
function Admin(props) {
    //const { clientName , compras} = props;

    const [clientName, setClientName] = useState(clientNameFromLocalStorage);
    const [compras, setCompras] = useState(comprasFromLocalStorage);
    const [cart, setCart] = useState(cartFromLocalStorage);
    const [page, setPage] = useState(PAGE_PRODUCTS);

    useEffect(() => {

        localStorage.setItem("compras", JSON.stringify(cart));
        localStorage.setItem("clientName", JSON.stringify(clientName));

    }, [cart, compras, clientName]);

    const navigateTo = (nextPage) => {
        setPage(nextPage);
    };

    const getComprasTotal = () => {
        return compras.reduce((sum, { quantity }) => sum + quantity, 0);
    };


    return (<div className="" >
        <header >
            <button onClick={
                () => navigateTo(PAGE_PURCHASES)
            } >
                Compras({getComprasTotal()}) {" "} </button> {" "}

            <button onClick={
                () => navigateTo(PAGE_PRODUCTS)
            } >
                Produtos {" "} </button>{" "}  </header >
        {" "}
        {
            page === PAGE_PRODUCTS && < Produtos cart={compras}
                setCart={setCart}
            />}{" "}

        {
            page === PAGE_PURCHASES && <Compras compras={compras}> </Compras>
        } {" "}   </div >
    );

}
function mapStateToProps(state) {
    return {
        clientName: state.purchases.clientName,
        compras: state.purchases.compras,

    }
}
export default connect(mapStateToProps)(Admin);