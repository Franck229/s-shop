
import React, { useState } from "react";
import Compras from "./Compras";

const PAGE_PURCHASES = "compras";

const clientNameBufferFromLocalStorage = JSON.parse(localStorage.getItem("clientNameBuffer") || "[]");
export default function Cart({ cart, setCart }) {

  const [clientNameBuffer, setClientNameBuffer] = useState(clientNameBufferFromLocalStorage);
  const [page, setPage] = useState();

  var clientNameTemp = "";
  const navigateTo = (nextPage) => {
    setPage(nextPage);
  };
  const getTotalSum = () => {
    return cart.reduce(
      (sum, { cost, quantity }) => sum + cost * quantity,
      0
    );
  };

  const clearCart = () => {
    setCart([]);
  };

  const setQuantity = (product, amount) => {
    const newCart = [...cart];
    newCart.find(
      (item) => item.name === product.name
    ).quantity = amount;
    setCart(newCart);
  };

  const removeFromCart = (productToRemove) => {
    setCart(
      cart.filter((product) => product !== productToRemove)
    );
  };
  const HandleChangeName = (newNome) => {

    clientNameTemp = newNome;
  };




  return (
    <>

      {page !== PAGE_PURCHASES ? <div className="">

        <h1>Cesta</h1>
        {cart.length > 0 && (
          <button onClick={clearCart}>Limpar a cesta</button>
        )}

        <table class="table caption-top">

          <caption> <p> Nome </p>   <div>
            <input

              name="Name"
              type={Text}
              placeholder="Nome"
              onChange={(event) => HandleChangeName(event.target.value)}
            /> </div></caption>

          <thead>
            <tr>
              <th scope="col">Qtd</th>
              <th scope="col">Produto</th>
              <th scope="col">Imagem</th>
              <th scope="col">Descrição</th>
              <th scope="col">Valor</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {cart.map((product, idx) => (
              <tr>
                <td>        <input
                  value={product.quantity}
                  onChange={(e) =>
                    setQuantity(
                      product,
                      parseInt(e.target.value)
                    )
                  }
                /> </td>
                <th scope="row" key={idx} >{product.name}</th>
                <td>  <img src={product.image} alt={product.name} width="50" /></td>

                <td>{product.description}</td>
                <td>${product.cost}</td>
                <td>
                  <button onClick={() => removeFromCart(product)}>
                    Remove
                  </button></td>


              </tr>
            ))}
          </tbody>



        </table>
        <div>Total : R${getTotalSum()}</div>
      </div> : <div></div>


      }




      <div className="" >
        <header >
          {page !== PAGE_PURCHASES ?
            <button onClick={() => {

              if (clientNameTemp === "") {
                alert("O campo nome não pode ficar em branco");
              } else {

                setClientNameBuffer(clientNameTemp);

                navigateTo(PAGE_PURCHASES)
                alert("compra realzada com sucesso")

              }


            }}>
              Finalizar Compra
            </button> : <div></div>

          }

          {" "}  </header >
        {" "}
        {
          page === PAGE_PURCHASES && <Compras compras={cart} clientName={clientNameBuffer}
          />}{" "}   </div >

    </>
  );
}
