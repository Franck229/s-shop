import React from "react";
import { connect } from 'react-redux'

const PurchasesTable = props => {
    const { clientName, compras } = props;
    const getTotalSum = () => {
        return compras.reduce(
            (sum, { cost, quantity }) => sum + cost * quantity,
            0
        );
    };
    return (
        <div>
            <h1>Compras</h1>
            <table class="table caption-top">
                <thead>
                    <tr>
                        <th scope="col">Nome do cliente</th>
                        <th scope="col">Produto</th>
                        <th scope="col">Imagem</th>
                        <th scope="col">Qtd</th>

                        <th scope="col">Total</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {compras.map((compra, idx) => (
                        <tr>
                            <td>{clientName}</td>
                            <th scope="row" key={idx} >{compra.name}</th>
                            <td>  <img src={compra.image} alt={compra.name} width="50" /></td>
                            <td>{compra.quantity}</td>
                            <td>R${getTotalSum()}          </td>

                        </tr>
                    ))}
                </tbody>



            </table>

        </div>
    )

}

function mapStateToProps(state) {
    return {
        clientName: state.purchases.clientName,
        compras: state.purchases.compras,

    }
}
export default connect(mapStateToProps)(PurchasesTable);