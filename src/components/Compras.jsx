import React from 'react';
import PurchasesTable from './PurchasesTable';
import { connect } from 'react-redux'
import { alterPurchasesClientData, alterPurchasesData } from '../store/actions/datas'
function Compras(props) {
  const { clientName, compras } = props;

  return (
    <>
      <PurchasesTable clientName={props.changeClient(clientName)} compras={props.changePurchasesData(compras)}
      ></PurchasesTable>

    </>
  );
}

/* function mapStateToProps(state) {
  return {
    clientName: state.purchases.clientName,
    compras: state.purchases.compras,

  }
} */
function mapDispatchToProp(dispatch) {
  return {
    changeClient(newPurchasesClient) {
      //Action Creator -> action
      const action = alterPurchasesClientData(newPurchasesClient)
      dispatch(action)

    },

    changePurchasesData(newPurchases) {
      //Action Creator -> action
      const action = alterPurchasesData(newPurchases)
      dispatch(action)


    }

  };
}
export default connect(/* mapStateToProps, */null, mapDispatchToProp)(Compras);